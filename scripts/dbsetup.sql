create database if not exists sampledb ;

create user if not exists 'testuser'@'localhost' identified by 'testuser' ;
grant all on sampledb.* to 'testuser'@'localhost' ;

use sampledb;

CREATE TABLE if not exists `m_emp` (
  `empno` bigint(20) NOT NULL AUTO_INCREMENT,
  `empname` varchar(255) DEFAULT NULL,
  `departmentid` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`empno`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ;

delete from sampledb.m_emp ;
INSERT INTO sampledb.m_emp (empname,departmentid) 
VALUES
 ('従業員A','10101001')
,('従業員B','10101001')
,('従業員C','10101002')
,('従業員D','10101003') ;
commit ;
