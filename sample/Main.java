// Main.java
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.*;
import com.ninja_squad.dbsetup.operation.*;

public class Main {
    public static void main(String[] args){
        System.out.println("hello, world!");

        Main main = new Main();
        main.mainProc();
    }

    public void mainProc(){
        Destination destination = new DriverManagerDestination("jdbc:mysql://localhost:3306/sampledb", "testuser", "testuser");

        //
        Operation operation =
        Operations.sequenceOf(
            // CommonOperations.DELETE_ALL,
            // CommonOperations.INSERT_REFERENCE_DATA,
            Operations.insertInto("m_emp")
                       .columns("EMPNAME")
                       .values("山田 太郎")
                       .build());
        DbSetup dbSetup = new DbSetup(destination, operation);
        dbSetup.launch();
    }
}